FROM postgres:12.4

RUN apt-get update && apt-get upgrade && apt-get -y install postgis

COPY ./init/initdb-extensions.sh /docker-entrypoint-initdb.d/initdb-extensions.sh

# Wheely app

## Technology stack
Ruby, Sinatra, Dry-db, Rom-db, postgresql, postgis.


## Basic installation

Create gemset and install gems:

```
  rvm ruby-2.7.1 do rvm gemset create wheely
  rvm use ruby-2.7.1@wheely

  gem install bundler

  bundle install
```

Run application using docker:

Docker-compose should build all the necessary images, create data files for postgres and fill database with small testing data.
```
  docker-compose up
```

Run application:

```
  bundle exec thin start -p 3000
```

Run workaround which is alternative to `rails c` command:

```
  bundle exec bin/console.rb
```

Run tests
```
bundle exec rpsec spec/
```

## Basic usage

Call `predict` method

```
  curl -X POST -H 'Content-Type: application/json' -d '{"target": {"lat": 55.789577, "lng": 37.55831}, "source": [{"lat":55.755266, "lng":37.633464}]}' 'localhost:3000/api/predict'

  # [5]
```

Call 'cars' method

```
  curl 'localhost:3001/api/cars?lat=55.736602&lng=37.58227&limit=2'
  # [{"id":2,"lat":55.789577,"lng":37.55831},{"id":1,"lat":55.755266,"lng":37.633464}]

```

Call 'nearest_car'

```
  curl 'localhost:3001/api/nearest_car?lat=55.736602&lng=37.58227'
  # 3
```

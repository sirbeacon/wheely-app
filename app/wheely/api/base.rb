require 'sinatra/base'
require 'sinatra/json'

module Wheely
  module Api
    class Base < Sinatra::Application
      get '/_health' do
        status 200
      end

      private

      def render_from_result(result)
        if result.success?
          status 200
          json result.value!
        else
          status 400
          json result.failure.errors.to_h
        end
      end
    end
  end
end

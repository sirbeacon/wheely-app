require_relative 'base'

module Wheely
  module Api
    class Cars < Base
      include Import['operations.get_nearest_cars']

      get '/' do
        render_from_result(get_nearest_cars.(params))
      end
    end
  end
end

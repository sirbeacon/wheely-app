require_relative 'base'

module Wheely
  module Api
    class NearestCar < Base
      include Import['operations.get_nearest_car']

      get '/' do
        render_from_result(get_nearest_car.(params))
      end
    end
  end
end

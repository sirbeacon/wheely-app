require_relative 'base'

module Wheely
  module Api
    class Predict < Base
      include Import['operations.get_avr_time',]

      post '/' do
        render_from_result(get_avr_time.(JSON.parse(request.body.read, symbolize_names: true)))
      end
    end
  end
end

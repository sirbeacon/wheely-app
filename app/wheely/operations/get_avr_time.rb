require 'dry/monads/result'
require 'dry/schema'

module Wheely
  module Operations
    class GetAvrTime
      include Dry::Monads::Result::Mixin
      include Import['persistence.repos.car_repo', 'services.get_time_for_car']

      Schema = Dry::Schema.Params do
        required(:target).hash do
          required(:lng).filled(:float, gt?: -180.0, lt?: 180.0)
          required(:lat).filled(:float, gt?: -90.0, lt?: 90.0)
        end
        required(:source).array(:hash) do
          required(:lng).filled(:float, gt?: -180.0, lt?: 180.0)
          required(:lat).filled(:float, gt?: -90.0, lt?: 90.0)
        end
      end

      def call(params)
        avr_speed = car_repo.get_avr_speed
        Schema.(params).to_monad
          .fmap do |res|
          res = res.to_h
          res[:source].map do |el|
            get_time_for_car.(res[:target][:lat], res[:target][:lng], el[:lat], el[:lng], avr_speed)
          end
        end
      end
    end
  end
end

require 'dry/monads/result'
require 'dry/schema'

module Wheely
  module Operations
    class GetNearestCar
      include Dry::Monads::Result::Mixin
      include Import['operations.get_nearest_cars',
                     'operations.get_avr_time',
                     'app.config']

      def call(params)
        get_nearest_cars.(params.merge(limit: config['wheely']['limit']))
          .bind { |res| get_avr_time.(target: params, source: res) }
          .fmap(&:min)
      end
    end
  end
end

require 'dry/monads/result'
require 'dry/schema'

module Wheely
  module Operations
    class GetNearestCars
      include Dry::Monads::Result::Mixin
      include Import['persistence.repos.car_repo']

      Schema = Dry::Schema.Params do
        required(:lng).filled(:float, gt?: -180.0, lt?: 180.0)
        required(:lat).filled(:float, gt?: -90.0, lt?: 90.0)
        required(:limit).filled(:integer, gt?: 0, lt?: 100)
      end

      def call(params)
        Schema.(params).to_monad
          .fmap { |res| car_repo.get_nearest_cars(res.to_h[:lng], res.to_h[:lat], res.to_h[:limit]) }
          .fmap { |res| res.to_a.map { |el| { id: el.id, lat: el.latitude, lng: el.longitude } } }
      end
    end
  end
end

module Wheely
  module Services
    class GetTimeForCar
      MINUTES_IN_HOUR = 60
      EARTH_RADIUS = 6371                   # Earth radius in kilometers

      def call(lat1, lon1, lat2, lon2, speed)
        (distance(lat1, lon1, lat2, lon2) / speed * MINUTES_IN_HOUR).round
      end

      # Haversine formula
      def distance(lat1, lon1, lat2, lon2)
        rad_per_deg = Math::PI / 180

        dlat_rad = (lat1 - lat2) * rad_per_deg  # Deltas, converted to rad
        dlon_rad = (lon1 - lon2) * rad_per_deg

        lat1_rad = lat1 * rad_per_deg
        lat2_rad = lat2 * rad_per_deg

        a = Math.sin(dlat_rad / 2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad / 2)**2
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        EARTH_RADIUS * c
      end
    end
  end
end

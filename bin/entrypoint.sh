#!/bin/bash

set -e

/opt/app/wheely/bin/wait_for_postgres.sh

bundle exec thin start -e production -p 3000

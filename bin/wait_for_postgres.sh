#!/bin/bash

set -e

export PGHOST=$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0])["db"]["host"]' /opt/app/wheely/config/config.yml)
export PGUSER=$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0])["db"]["username"]' /opt/app/wheely/config/config.yml)
export PGPASSWORD=$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0])["db"]["password"]' /opt/app/wheely/config/config.yml)
export PGDATABASE=$(ruby -r yaml -e 'puts YAML.load_file(ARGV[0])["db"]["dbname"]' /opt/app/wheely/config/config.yml)

echo $PGHOST
echo $PGUSER
echo $PGPASSWORD

until pg_isready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up"

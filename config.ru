require_relative 'system/boot'
map('/api') { run Wheely::Api::Base }
map('/api/cars') { run Wheely::Api::Cars }
map('/api/nearest_car') { run Wheely::Api::NearestCar }
map('/api/predict') { run Wheely::Api::Predict }

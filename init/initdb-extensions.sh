#!/bin/sh

set -e

psql --username "$POSTGRES_USER" --dbname="$POSTGRES_DB" <<-EOSQL
  CREATE EXTENSION IF NOT EXISTS postgis;
  CREATE EXTENSION IF NOT EXISTS postgis_topology;
  CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
  CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;

  CREATE TABLE persistence_relations_cars (
    id          serial PRIMARY KEY,
    the_geog    geography(POINT,4326),
    name        varchar(100),
    avr_speed   float8,
    longitude   float8,
    latitude    float8
  );

  CREATE INDEX cars_the_geog_index ON persistence_relations_cars USING GIST ( the_geog );

  insert into persistence_relations_cars (the_geog, name, avr_speed, longitude, latitude) values(ST_GeographyFromText('SRID=4326;POINT(55.755266 37.633464)'), 'AUDI (Kitay gorod)', 80, 37.633464, 55.755266);
  insert into persistence_relations_cars (the_geog, name, avr_speed, longitude, latitude) values(ST_GeographyFromText('SRID=4326;POINT(55.789577 37.558310)'), 'BMW (Dinamo)', 80, 37.558310, 55.789577);

EOSQL

require 'rom'

module Persistence
  module Relations
    class Cars < ROM::Relation[:sql]
      schema do
        attribute :id, Types::Float
        attribute :name, Types::String
        attribute :avr_speed, Types::Float
        attribute :longitude, Types::Float
        attribute :latitude, Types::Float
      end
    end
  end
end

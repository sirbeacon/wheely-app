require 'rom'

module Persistence
  module Repos
    class CarRepo < ROM::Repository[:persistence_relations_cars]
      include Wheely::Import['container', 'gateways.memcache']

      def all
        persistence_relations_cars.to_a
      end

      def get_nearest_cars(longitude, latitude, limit)
        persistence_relations_cars.read <<-SQL
          SELECT * 
          FROM   persistence_relations_cars 
          ORDER BY the_geog <-> 'SRID=4326;POINT(#{latitude} #{longitude})'::geometry 
          LIMIT #{limit}
        SQL
      end

      def get_avr_speed
        memcache.cache 'avr_speed' do
          persistence_relations_cars.avg(:avr_speed)
        end
      end
    end
  end
end

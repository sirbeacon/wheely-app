require_relative '../system/wheely/container'
require 'rack/test'

Wheely::Container.finalize!

describe 'Api tests' do
  include Rack::Test::Methods

  def app
    Wheely::Container['api.base']
  end

  it 'checks health method' do
    get '/_health' do
      expect(last_response.status).to eq(200)
    end
  end
end

require_relative '../system/wheely/container'
require 'rack/test'

Wheely::Container.finalize!

describe 'Tests for getting cars' do
  include Rack::Test::Methods

  def app
    Wheely::Container['api.cars']
  end

  it 'gets success response' do
    get '/cars', lng: 37.571694, lat: 55.779995, limit: 1
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body)).to eq([{ 'id' => 2, 'lat' => 55.789577, 'lng' => 37.55831 }])
  end

  it 'warns about wrong params' do
    get '/cars', lng: 999_999, lat: 'lat'
    expect(last_response.status).to eq(400)
    expect(JSON.parse(last_response.body)).to eq({ 'lat' => ['must be a float'],
                                                   'limit' => ['is missing'],
                                                   'lng' => ['must be less than 180.0'] })
  end
end

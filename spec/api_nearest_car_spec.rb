require_relative '../system/wheely/container'
require 'rack/test'

Wheely::Container.finalize!

describe 'Tests for getting closest car' do
  include Rack::Test::Methods

  def app
    Wheely::Container['api.nearest_car']
  end

  it 'gets success response' do
    get '/nearest_car', lat: 55.736602, lng: 37.582276
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body)).to eq(3)
  end

  it 'warns about wrong params' do
    get '/nearest_car', lat: 999_999, lng: 'msg'
    expect(last_response.status).to eq(400)
    expect(JSON.parse(last_response.body)).to eq({ 'lat' => ['must be less than 90.0'],
                                                   'lng' => ['must be a float'] })
  end
end

require_relative '../system/wheely/container'
require 'rack/test'

Wheely::Container.finalize!

describe 'Tests for getting times for coordinates' do
  include Rack::Test::Methods

  def app
    Wheely::Container['api.predict']
  end

  it 'gets success response' do
    data = '{"target": {"lat": 55.789577, "lng": 37.55831}, "source": [{"lat":55.755266, "lng":37.633464}]}'
    post '/predict', data, { 'CONTENT_TYPE' => 'application/json' }
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body)).to eq([5])
  end

  it 'warns about wrong params' do
    data = '{"target": {"lat": 999999, "lng": "lng"}, "source": [{"lat":"lat", "lng":99999}]}'
    post '/predict', data, { 'CONTENT_TYPE' => 'application/json' }
    expect(last_response.status).to eq(400)
    expect(JSON.parse(last_response.body)).to eq({ 'source' => { '0' => { 'lat' => ['must be a float'],
                                                                          'lng' => ['must be less than 180.0'] } },
                                                   'target' => {
                                                       'lat' => ['must be less than 90.0'],
                                                       'lng' => ['must be a float']
                                                   } })
  end
end

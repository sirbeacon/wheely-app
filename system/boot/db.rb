Wheely::Container.boot(:db) do
  init do
    require 'yaml'
    require 'rom'
    require 'rom-sql'

    config = YAML.load_file('config/config.yml')
    register('app.config', config)

    opts = {
      username: config['db']['username'],
      password: config['db']['password'],
      encoding: 'UTF8'
    }

    register('db.config',
             ROM::Configuration.new(:sql, "postgres://#{config['db']['host']}/#{config['db']['dbname']}", opts))
  end
end

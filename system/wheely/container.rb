require 'dry/system/container'
require 'dry/system/components'
require 'dry/schema'
require 'dalli'

Dry::Schema.load_extensions(:monads)

module Wheely
  class Container < Dry::System::Container
    use :logging
    use :env, inferrer: -> { ENV.fetch('RACK_ENV', :development).to_sym }

    configure do |config|
      config.default_namespace = 'wheely'
      config.logger = Logger.new($stdout)
      config.auto_register = %w[app lib]
    end

    load_paths!('app', 'system', 'lib')

    register(:memcache) do
      Dalli::Client.new(Wheely::Container['app.config']['memcached']['host'],
                        expires_in: Wheely::Container['app.config']['memcached']['expires_in'],
                        compress: true)
    end
  end

  Import = Container.injector
end
